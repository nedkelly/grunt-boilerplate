# Basic Auto Grid System

Auto grids extend the built in basic semantic grid system with the addition of a CSS Quantity Query based option.

For a working example see the <a href="http://codepen.io/nedkelly/pen/dGVxOp" target="_blank">CodePen</a> demo, also see the <a href="http://codepen.io/nedkelly/pen/WrEyKo/" target="_blank">Basic Semantic Grid</a>.

<p data-height="268" data-theme-id="22944" data-slug-hash="dGVxOp" data-default-tab="css" data-user="nedkelly" class="codepen">See the Pen <a href="http://codepen.io/nedkelly/pen/dGVxOp/">Basic Grid System</a> by Nathan Kelly (<a href="http://codepen.io/nedkelly">@nedkelly</a>) on <a href="http://codepen.io">CodePen</a>.</p>
<script async src="//assets.codepen.io/assets/embed/ei.js"></script>

### Auto Columns Usage

You can use the supplied mixin `auto-columns()` to create simple CSS Quantity Query based grids, these are especially useful when dealing with content that might vary dynamically from time to time.

Specify you own markup:

``` html
<div class="my-qq-grid">
  <div class="row">
    <div class="column"><div class="content">text</div></div>
    <div class="column"><div class="content">text</div></div>
  </div>
</div>
```

Then add the SCSS:

``` scss
.my-qq-grid {
  @include breakpoint(md) {
    @include auto-columns(3);
  }
}
```

_The `auto-columns()` mixin takes a single optional argument `$max` to allow you to limit the amount of CSS generated, so if you know the maximum number of columns be sure to set it when calling the mixin to avoid un-needed CSS._

The CSS result:

``` css
@media (min-width: 769px) {
  .my-qq-grid .column:nth-last-child(n+2):first-child,
  .my-qq-grid .column:nth-last-child(n+2):first-child ~ .column {
    width: 50%;
  }

  .my-qq-grid .column:nth-last-child(n+3):first-child,
  .my-qq-grid .column:nth-last-child(n+3):first-child ~ .column {
    width: 33.33333%;
  }
}
```

Keep your output to a minimum, try to produce only what you need to, it is easy to go overkill with this technique, keep it real...
