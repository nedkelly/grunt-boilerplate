<fieldset>
  <legend>Headings</legend>

  <h1>Heading level 1</h1>
  <p>Use this mark-up to ensure that all the elements are styled in your project.</p>

  <h2>Heading level 2</h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum optio veniam doloremque ratione reiciendis, culpa eius illo ullam libero possimus eum repellat iste, praesentium molestias ducimus consequuntur debitis sit deleniti?</p>

  <h3>Heading level 3</h3>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem illo nostrum delectus, blanditiis! Ipsam nisi labore repellat ut quo provident voluptatum tempora, suscipit quod? Voluptatem minus quam perferendis vitae.</p>

  <h4>Heading level 4</h4>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, possimus? Earum magnam aliquam dolorum pariatur, nemo amet neque commodi quibusdam ea dignissimos, culpa? Doloremque, voluptas? Sit est animi, reprehenderit ab!</p>

  <h5>Heading level 5</h5>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic atque minima minus voluptates error mollitia similique aliquam provident eveniet nesciunt accusamus possimus culpa suscipit facere assumenda libero, debitis reprehenderit eum.</p>

  <h6>Heading level 6</h6>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic atque minima minus voluptates error mollitia similique aliquam provident eveniet nesciunt accusamus possimus culpa suscipit facere assumenda libero, debitis reprehenderit eum.</p>
</fieldset>
<br>

<fieldset>
  <legend>Typography</legend>

  <p>Regular paragraph. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet distinctio adipisci omnis provident? Praesentium libero non nulla iusto, perspiciatis consectetur explicabo aliquid quisquam voluptatum reprehenderit magnam, commodi esse facere. Voluptatem.</p>

  <p>Let's start with a really nice <a href="#">example of a hyperlink</a>.</p>

  <p>Here we have some <strong>bold text using the &lt;strong&gt; tag</strong> and this is some <b>more bold text using the &lt;b&gt; tag</b></p>

  <p>Now we have some <em>emphasised text using the &lt;em&gt; tag</em> and some more <i>emphasised text using the &lt;i&gt; tag</i>, cool?</p>

  <p>Lets use the &lt;s&gt; tag to <s>put a strike through the text</s>. We could also use the &lt;del&gt; tag to <del>mark text as deleted</del> or use the &lt;ins&gt; tag to show that <ins>something has been added</ins>.</p>

  <p>Here we have some <mark>highlighted text</mark>, pretty neat.</p>

  <p>Here we use the &lt;q&gt; tag to show a short quote: <q>Here's a short quote</q>.</p>

  <p>We could also use the &lt;small&gt; tag to <small>make some text smaller</small> than the rest of the paragraph.</p>

  <p>We have the &lt;sub&gt; tag, which allows us to put some sub<sub>text</sub> to text. We also have the &lt;sup&gt; for putting super<sup>scripts</sup> to words.</p>

  <p>Who can forget the <u>underline</u> tag?</p>

  <blockquote>
    <p>Blockquotes are NOT to be forgotten. They're awesome.</p>
    <footer>- <cite>Blockquote author</cite></footer>
  </blockquote>

  <pre><code>
// This is a code block.
#header h1 a {
  display: block;
  width: 300px;
  height: 80px;
}
  </code></pre>
</fieldset>
<br>

<fieldset>
  <legend>Lists</legend>

  <h2>Ordered lists</h2>
  <ol>
    <li>Ordered item 1</li>
    <li>Ordered item 2</li>
    <li>Ordered item 3</li>
    <li>Ordered item 4</li>
  </ol>

  <h2>Unordered lists</h2>
  <ul>
    <li>Ordered item 1</li>
    <li>Ordered item 2</li>
    <li>Ordered item 3</li>
    <li>Ordered item 4</li>
  </ul>

  <h2>Definition lists</h2>
  <dl>
    <dt>Title</dt>
    <dd>Description</dd>

    <dt>Another title</dt>
    <dd>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dolore, earum quidem error officia explicabo voluptatibus ab voluptas eligendi, fuga excepturi dolor. Totam tempore nihil repellendus officia nisi architecto fuga!</dd>
  </dl>
</fieldset>
<br>

<fieldset>
  <legend>Tables</legend>

  <table>
    <thead>
      <tr>
        <th>Heading 1</th>
        <th>Heading 2</th>
        <th>Heading 3</th>
        <th>Heading 4</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Table cell content 1</td>
        <td>Table cell content 2</td>
        <td>Table cell content 3</td>
        <td>Table cell content 4</td>
      </tr>
      <tr>
        <td>Table cell content 1</td>
        <td>Table cell content 2</td>
        <td>Table cell content 3</td>
        <td>Table cell content 4</td>
      </tr>
      <tr>
        <td>Table cell content 1</td>
        <td>Table cell content 2</td>
        <td>Table cell content 3</td>
        <td>Table cell content 4</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td>Table foot 1</td>
        <td>Table foot 2</td>
        <td>Table foot 3</td>
        <td>Table foot 4</td>
      </tr>
    </tfoot>
  </table>
</fieldset>
<br>

<fieldset>
  <legend>Forms</legend>

  <form action="#">
    <p>
      <input type="button" id="button-field" value="Input type of button field">
    </p>
    <p>
      <input type="checkbox" id="checkbox">
      <label for="checkbox">Label for checkbox</label>
    </p>
    <p>
      <label for="date-field">Label for date field</label>
      <input type="date" id="date-field" placeholder="Date field">
    </p>
    <p>
      <label for="email-field">Label for email field</label>
      <input type="email" id="email-field" placeholder="Email field">
    </p>
    <p>
      <label for="file-field">Label for file field</label>
      <input type="file" id="file-field">
    </p>
    <p>
      <label for="month-field">Label for month field</label>
      <input type="month" id="month-field">
    </p>
    <p>
      <label for="number-field">Label for number field</label>
      <input type="number" id="number-field" placeholder="Number field">
    </p>
    <p>
      <label for="password-field">Label for password field</label>
      <input type="password" id="passworld-field" placeholder="Password field">
    </p>
    <p>
      <input type="radio" name="radio-group" id="radio-group-1"> <label for="radio-group-1">Label for radio item 1</label>
      <br>
      <input type="radio" name="radio-group" id="radio-group-2"> <label for="radio-group-2">Label for radio item 2</label>
    </p>
    <p>
      <label for="range-field">Label for range field</label>
      <input type="range" id="range-field">
    </p>
    <p>
      <input type="reset" value="Reset button">
      <input type="submit" value="Submit button">
    </p>
    <p>
      <label for="text-field">Label for text field</label>
      <input type="text" id="text-field" placeholder="Text field">
    </p>
    <p>
      <label for="time-field">Label for time field</label>
      <input type="time" id="time-field">
    </p>
    <p>
      <label for="url-field">Label for URL field</label>
      <input type="url" id="url-field">
    </p>

    <hr>

    <p>
      <button>This is a button tag</button>
    </p>
    <p>
      <select>
        <option>Dropdown item one</option>
        <option>Dropdown item two</option>
        <option>Dropdown item three</option>
      </select>
    </p>
    <p>
      <label for="textarea-field">Label for textarea field</label>
      <textarea placeholder="Textarea field" id="textarea-field"></textarea>
    </p>
  </form>
</fieldset>
