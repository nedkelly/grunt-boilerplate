
// Gruntfile

module.exports = function(grunt) {
  var pkg = grunt.file.readJSON('package.json');
  var path = require('path');

  // measures the time each task takes
  require('time-grunt')(grunt);

  // load grunt config
  require('load-grunt-config')(grunt, {
    configPath: path.join(process.cwd(), 'grunt/config'),
    jitGrunt: {
      staticMappings:{
        browserSync: 'grunt-browser-sync',
        //clean: 'grunt-contrib-clean',
        cmq: 'grunt-combine-media-queries',
        compress: 'grunt-contrib-compress',
        copy: 'grunt-contrib-copy',
        cssmin: 'grunt-cssmin',
        markdown: 'grunt-markdown',
        newer: 'grunt-newer',
        rename: 'grunt-rename',
        sass: 'grunt-sass',
        swig: 'grunt-swig-templates',
      }
    }
  });
};
