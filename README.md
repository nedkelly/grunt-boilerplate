# Grunt Boilerplate

Styleguide and codebase for all client-side code.

### What's in this build?

- [GruntJS](http://gruntjs.com/)
- [Swig](http://paularmstrong.github.io/swig/)
- [LibSass](http://libsass.org/) with [Bourbon mixin library](http://bourbon.io/)
- [Browsersync](http://www.browsersync.io/) with [Project Server](https://www.npmjs.com/package/project-server)

### Getting started

To get started, simply follow these simple steps:

###### 1. Clone the repository

``` bash
git clone git@bitbucket.org:nedkelly/grunt-boilerplate.git
cd grunt-boilerplate
```

###### 2. Install dependencies

``` bash
npm install -g grunt-cli
npm install
```

###### 3. Run the default grunt task

``` bash
grunt
```

### Available Grunt tasks

__*requires further updates*__

Run `grunt` followed by one of the tasks below to perform a specific action. Otherwise run `grunt` by itself to run the default task defined below.

- **build**   - `runs [css, js, templates, docs, newer:copy:fonts, newer:imagemin]`
- **build-dev** - `runs [sass, concat, templates, docs, newer:copy:fonts, newer:imagemin]`
- **clean**   - `runs [clean]`
  *(removes `./public` directory and it's contents)*
- **copy**    - `runs [copy]`
  *(copies fonts from `./src/assets/` to `./public/assets/`)*
- **css**     - `runs [sass, cmq, cssmin]`
  *(compiles SASS and minifies the source)*
- **default**   - `runs [build, serve, watch]`
  *(compiles the build with CSS and JS minified and production ready)*
- **dev**     - `runs [build-dev, serve, watch]`
  *(compiles the build without minifying or modifying the CSS or JS)*
- **docs**    - `runs [newer:markdown]`
  *(generate the documentation based on the markdown files in the `root` and `./src` directories, excludes `./node_modules` and any other directories in the `root`)*
- **js**    - `runs [concat, uglify]`
  *(concatenates and minifies JS files)*
- **package**   - `runs [rebuild, compress:package, rename:package]`
  *(zips up the `./public/assets` directory into the current name and version defined in package.json `<pkg.name>-<pkg.version>.zip`)*
- **rebuild**   - `runs [clean, build]`
- **redev**   - `runs [clean, build-dev]`
- **serve**   - `runs [browserSync]`
  *(runs a local server from the `./public/` directory and reloads the browser if changes are made)*
- **templates** - `runs [swig]`
  *(compiles swig templates into pages)*
- **watch**   - `runs [watch]`
  *(watches js, css, images, fonts and templates and runs correspondent task when a file is modified/created)*
