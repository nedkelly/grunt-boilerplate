
// CSS Minify config

module.exports = {
  options: {
    debug: false
  },
  target: {
    files: {
      'public/assets/css/screen.css': 'public/assets/css/screen.css'
    }
  }
};
