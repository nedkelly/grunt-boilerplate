
// Markdown Config

module.exports = {
  all: {
    files: [
    {
      expand: true,
      src: [
        './*.md',
        './src/**/*.md'
      ],
      dest: 'public/docs',
      ext: '.html',
      flatten: true
    }
    ],
    options: {
      template: 'grunt/templates/docs.html',
      templateContext: {
        favicon: '../img/charlie.jpg'
      }
    }
  }
};
