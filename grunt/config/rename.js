
// Rename Config

var pkg = require('../../package.json');

module.exports = {
  package: {
    src: pkg.name + '-' + pkg.version + '.zip',
    dest: 'public/' + pkg.name + '-' + pkg.version + '.zip'
  }
};
