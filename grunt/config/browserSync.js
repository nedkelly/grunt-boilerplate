
// Browsersync Config
var pkg = require('../../package.json');
var bsConfig = {
  input: './public/',
  useCustomIndex: true,
  projectServer: {
    defaultPage: '/docs/README.html',
    localStorageSeed: 'I love bacon',
    menus: [
      {
        title: 'Templates',
        path: '/'
      },
      {
        title: 'Documentation',
        path: '/docs'
      }
    ],
    pkg: {
      name: pkg.name,
      favicon: '/assets/img/charlie.jpg',
      version: pkg.version,
    }
  }
};

module.exports = {
  dev: {
    bsFiles: {
      src: [
        './public/**'
      ]
    },
    options: {
      watchTask: true,
      notify: false,
      online: false,
      open: "local",
      server: {
        baseDir: bsConfig.input,
        directory: !bsConfig.useCustomIndex,
        middleware: bsConfig.useCustomIndex !== false ? [require('project-server').projectServerIndex(bsConfig.input, { config: bsConfig.projectServer })]: '',
      }
    }
  }
};
