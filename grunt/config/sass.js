
// SASS config

module.exports = {
  options: {
    includePaths: require('node-bourbon').includePaths
  },
  all: {
    files: {
      'public/assets/css/screen.css': 'src/assets/css/screen.scss'
    }
  }
};
