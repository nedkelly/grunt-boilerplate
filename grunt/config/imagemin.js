
// Imagemin Config

module.exports = {
  all: {
    files: [{
      expand: true,
      cwd: 'src/assets/img',
      src: '**',
      dest: 'public/assets/img/'
    }]
  }
};
