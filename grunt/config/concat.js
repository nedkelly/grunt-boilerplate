
// Concat Config

module.exports = {
  libraries: {
    src: 'src/assets/js/libraries/**/*.js',
    dest: 'public/assets/js/libraries.js',
  },
  main: {
    src: 'src/assets/js/main/**/*.js',
    dest: 'public/assets/js/main.js',
  },
  polyfills: {
    src: 'src/assets/js/polyfills/**/*.js',
    dest: 'public/assets/js/polyfills.js',
  },
};
