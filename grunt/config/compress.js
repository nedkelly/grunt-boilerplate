
// Compress Config

var pkg = require('../../package.json');

module.exports = {
  package: {
    options: {
      mode: 'zip',
      archive: pkg.name + '-' + pkg.version + '.zip',
      pretty: true
    },
    expand: true,
    cwd: 'public/assets/',
    src: ['**/*']
  }
};
