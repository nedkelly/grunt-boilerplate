
// Copy Config

module.exports = {
  fonts: {
    expand: true,
    cwd: 'src/assets/fonts',
    src: '**',
    dest: 'public/assets/fonts/'
  }
};
