
// Watch config

module.exports = {
  css: {
    files: ['src/assets/css/**/*.scss'],
    tasks: ['css']
  },
  js: {
    files: ['src/assets/js/**/*.js'],
    tasks: ['js']
  },
  markdown: {
    files: [
      './*.md',
      './src/**/*.md'
    ],
    tasks: ['markdown']
  },
  swig: {
    files: [
      './src/pages/**/*.swig',
      './src/layouts/**/*.swig',
      './src/partials/**/*.swig'
    ],
    tasks: ['swig']
  },
  fonts: {
    files: ['src/assets/fonts/**/*.{eot,ttf,otf,woff,svg}'],
    tasks: ['copy:fonts']
  },
  images: {
    files: ['src/assets/img/**/*.{jpg,gif,png,svg}'],
    tasks: ['copy:images']
  },
  options: {
    spawn: false,
    interrupt: true,
    livereload: false,
  },
};
