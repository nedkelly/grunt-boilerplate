
// All tasks

module.exports = {
  'build':     ['css', 'js', 'templates', 'docs', 'newer:copy:fonts', 'newer:imagemin'],
  'build-dev': ['sass', 'concat', 'templates', 'docs', 'newer:copy:fonts', 'newer:imagemin'],
  'css':       ['sass', 'cmq', 'cssmin'],
  'default':   ['build', 'serve', 'watch'],
  'dev':       ['build-dev', 'serve', 'watch'],
  'docs':      ['newer:markdown'],
  'js':        ['concat', 'uglify'],
  'package':   ['rebuild', 'compress:package', 'rename:package'],
  'rebuild':   ['clean', 'build'],
  'redev':     ['clean', 'build-dev'],
  'serve':     ['browserSync'],
  'templates': ['swig'],
};
