
// Uglify Config

module.exports = {
  libraries: {
    files: {
      'public/assets/js/libraries.js': 'public/assets/js/libraries.js'
    }
  },
  main: {
    files: {
      'public/assets/js/main.js': 'public/assets/js/main.js'
    }
  },
  polyfills: {
    files: {
      'public/assets/js/polyfills.js': 'public/assets/js/polyfills.js'
    }
  },
};
