
// Combine Media Queries config

module.exports = {
  options: {
    log: false
  },
  target: {
    files: {
      'public/assets/css': 'public/assets/css/screen.css'
    }
  }
};
