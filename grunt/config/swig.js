
// Swig Config

module.exports = {
  dev: {
    options: {
      cache: false,
      varControls: ['{=', '=}']
    },
    expand: true,
    cwd: 'src/pages/',
    dest: 'public/',
    src: ['**/*.swig'],
    ext: '.html'
  }
}
