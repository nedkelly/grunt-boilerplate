/**
 * This script will enforce commit messages are following the DT/AngularJS commit messages.
 */

var inquirer = require('inquirer'),
	colors = require('gulp-util').colors,
	exec = require('child_process').exec;


/**
 * Simplified exec function that automatically handles errors.
 *
 * @param  {string}   command Command to run.
 * @param  {function} success Callback function that receives the stdout
 */
var cmd = function(command, success) {
	exec(command, function(error, stdout, stderr) {
		if (error !== null) {
			console.error('exec error: ' + error);
			console.error(stderr);
			return false;
		}
		
		success(stdout);
	});
};

var callback = function(answers) {
	var commands = [];
	
	if (answers.add === true) {
		commands.push('git add -A');
	}
	
	var commitMessage = answers.type + '(' + answers.scope + '): ' + answers.message;
	commands.push('git commit -m "' + commitMessage + '"');
	
	cmd(commands.join(' && '), function(stdout) {
		// Show the output that is returned from the commands that were run.
		// console.log(stdout);
		
		console.log(colors.green('✔ All done!'));
	});
};

var questions = [];

// Add all files before committing?
questions.push({
	type: 'confirm',
	name: 'add',
	message: 'Would you like to '+ colors.yellow('add all') +' the files before committing?',
	default: true
});

// Type of commit.
questions.push({
	type: 'list',
	name: 'type',
	message: 'Please select the '+ colors.yellow('type') +' of commit:',
	choices: [{
		name: 'feat:      ' + colors.grey('A new feature'),
		value: 'feat',
		short: 'feat'
	}, {
		name: 'fix:       ' + colors.grey('A bug fix'),
		value: 'fix',
		short: 'fix'
	}, {
		name: 'docs:      ' + colors.grey('Changes to the documentation only'),
		value: 'docs',
		short: 'docs'
	}, {
		name: 'style:     ' + colors.grey('Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)'),
		value: 'style',
		short: 'style'
	}, {
		name: 'refactor:  ' + colors.grey('A code change that neither fixes a bug or adds a feature'),
		value: 'refactor',
		short: 'refactor'
	}, {
		name: 'perf:      ' + colors.grey('A code change that improves performance'),
		value: 'perf',
		short: 'perf'
	}, {
		name: 'test:      ' + colors.grey('Adding missing tests'),
		value: 'test',
		short: 'test'
	}, {
		name: 'chore:     ' + colors.grey('Changes to the build process or auxiliary tools and libraries such as documentation generation'),
		value: 'chore',
		short: 'chore'
	}]
});

// Get the scope of the commit.
questions.push({
	type: 'input',
	name: 'scope',
	message: 'What is the '+ colors.yellow('scope') +' of your commit?',
	validate: function(input) {
		if (input.length < 3) {
			return 'Please enter a scope (3 or more characters).';
		}
		
		return true;
	}
});

// Get the commit message.
questions.push({
	type: 'input',
	name: 'message',
	message: 'Please enter your '+ colors.yellow('commit message') +':',
	validate: function(input) {
		if (input.length < 3) {
			return 'Please enter a longer commit message.';
		}
		
		return true;
	}
});


inquirer.prompt(questions, callback);
